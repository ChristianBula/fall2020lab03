//Oleg Christian Bula 1935095

package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	void getTest() {
		Vector3d v = new Vector3d(5,16,7);
		assertEquals(5, v.getX());
        assertEquals(16, v.getY());
        assertEquals(7, v.getZ());
	}
    
	@Test
    void magnitudeTest() {
        Vector3d v = new Vector3d(2,5,3);
        assertEquals(6.164414002968976, v.magnitude());   
    }
	@Test
	void dotProductTest() {
		Vector3d v = new Vector3d(4,1,5);
		Vector3d v2 = new Vector3d(3,2,10);
		assertEquals(64,v.dotProduct(v2));
	}
	@Test
	void addTest() {
		Vector3d v = new Vector3d(4,1,5);
		Vector3d v2 = new Vector3d(3,2,10);
		Vector3d v3 = v.add(v2);
		assertEquals(7,v3.getX());
		assertEquals(3,v3.getY());
		assertEquals(15,v3.getZ());
	}
}
