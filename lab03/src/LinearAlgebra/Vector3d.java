//Oleg Christian Bula 1935095

package LinearAlgebra;
import java.lang.Math;

public class Vector3d {
    final private double x;
    final private double y;
    final private double z;
    
    public static void main(String[] args) {
    	Vector3d v = new Vector3d(4,1,5);
		Vector3d v2 = new Vector3d(3,2,10);
		System.out.println(v.dotProduct(v2));
    }
    
    public Vector3d(double x,double y,double z) {
    	this.x = x;
      	this.y = y;
       	this.z = z;
    }
    public double getX() {
    	return x;
    }
    public double getY() {
    	return y;
    }
    public double getZ() {
    	return z;
    }
    public double magnitude() {
    	double magnitude = Math.sqrt((x*x)+(y*y)+(z*z));
    	return magnitude;
    }
    public double dotProduct(Vector3d vector2){
    	double dot = ((this.x * vector2.x)+(this.y * vector2.y)+(this.z*vector2.z));
    	return dot;
    }
    public Vector3d add(Vector3d vector2) {    	
    	double x2 = this.x + vector2.x;
    	double y2 = this.y + vector2.y;
    	double z2 = this.z + vector2.z;
    	
        Vector3d v = new Vector3d(x2,y2,z2);
        return v;
    }
}
